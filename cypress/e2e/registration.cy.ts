describe("Registration test", () => {
  it("create new user", () => {
    cy.intercept("POST", "/api/v1/user").as("createuser");

    cy.visit("/signup");

    cy.fixture("user").then((user) => {
      cy.get('[data-test="username-input"]').type(user.name);
      cy.get('[data-test="password-input"]').type(user.password);
    });

    cy.get('[data-test="email-input"]').type("nonexisting@example.org");

    cy.get('[data-test="submit"]').click();

    cy.wait("@createuser");
    cy.get("@createuser").its("response.statusCode").should("eq", 201);
  });
});
