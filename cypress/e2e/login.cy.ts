describe("App login test", () => {
  it("redirect to login page", () => {
    cy.visit("/");

    cy.contains("Login");
  });

  it("login with correct user", () => {
    cy.intercept("POST", "/api/v1/user/login").as("authenticate");

    cy.visit("/login");

    cy.fixture("readyuser").then((user) => {
      cy.get('[data-test="username-input"]').type(user.name);
      cy.get('[data-test="password-input"]').type(user.password);
    });

    cy.get('[data-test="submit"]').click();

    cy.wait("@authenticate");
    cy.get("@authenticate").its("response.statusCode").should("eq", 200);
  });

  it("login with incorrect user", () => {
    cy.intercept("POST", "/api/v1/user/login").as("authenticate");

    cy.visit("/login");

    cy.get('[data-test="username-input"]').type("aaaaaaaaaaaaaaaa");
    cy.get('[data-test="password-input"]').type("bbbbbbbbbbbbbbbb");

    cy.get('[data-test="submit"]').click();

    cy.wait("@authenticate");
    cy.get("@authenticate").its("response.statusCode").should("eq", 401);
  });
});
