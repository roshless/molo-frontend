import { createRouter, createWebHistory } from "vue-router";
import { useAuthStore } from "@/stores/auth";

const Login = () => import("@/views/LoginView.vue");
const AddFeed = () => import("@/views/AddFeed.vue");
const Settings = () => import("@/views/SettingsView.vue");
const FeedsList = () => import("@/views/FeedsList.vue");
const NotFound = () => import("@/views/NotFound.vue");
const SignUp = () => import("@/views/SignUp.vue");
const UnreadFeedList = () => import("@/views/UnreadEntriesList.vue");
const SpecificFeed = () => import("@/views/SpecificFeed.vue");
const Logout = () => import("@/views/LogoutView.vue");
const AdminPanel = () => import("@/views/AdminPanel.vue");
const DeleteFeedView = () => import("@/views/DeleteFeedView.vue");
const ExportView = () => import("@/views/ExportView.vue");
const ImportView = () => import("@/views/ImportView.vue");

const routes = [
  {
    path: "/",
    name: "all-unread-feeds",
    component: UnreadFeedList,
    meta: { requiresAuth: true },
  },
  {
    path: "/feed/:id",
    name: "show-feed",
    component: SpecificFeed,
    meta: { requiresAuth: true },
  },
  {
    path: "/feed/:id/delete",
    name: "delete-feed",
    component: DeleteFeedView,
    meta: { requiresAuth: true },
  },
  {
    path: "/feeds",
    name: "list-feeds",
    component: FeedsList,
    meta: { requiresAuth: true },
  },
  {
    path: "/add",
    name: "add-feed",
    component: AddFeed,
    meta: { requiresAuth: true },
  },
  {
    path: "/settings",
    name: "settings",
    component: Settings,
    meta: { requiresAuth: true },
  },
  {
    path: "/signup",
    name: "signup",
    component: SignUp,
    meta: { requiresAuth: false },
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: { requiresAuth: false },
  },
  {
    path: "/logout",
    name: "logout",
    component: Logout,
    meta: { requiresAuth: true },
  },
  {
    path: "/admin",
    name: "admin-panel",
    component: AdminPanel,
    meta: { requiresAuth: true },
  },
  {
    path: "/export",
    name: "export-feeds",
    component: ExportView,
    meta: { requiresAuth: true },
  },
  {
    path: "/import",
    name: "import-feeds",
    component: ImportView,
    meta: { requiresAuth: true },
  },
  {
    path: "/:pathMatch(.*)*",
    name: "not-found",
    component: NotFound,
    meta: { requiresAuth: false },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
  linkActiveClass: "is-active",
  linkExactActiveClass: "is-active",
});

router.beforeEach(async (to) => {
  const store = useAuthStore();

  if (store.isTokenExpired()) {
    store.clearToken();
    return { name: "logout" };
  }

  if (!store.isLoggedIn() && to.meta.requiresAuth) {
    return {
      name: "login",
      query: { redirect: to.fullPath },
    };
  }

  // Not possible via UI, but just in case
  if (store.isLoggedIn() && (to.name === "login" || to.name === "signup")) {
    return {
      name: "list-files",
    };
  }
});

export default router;
