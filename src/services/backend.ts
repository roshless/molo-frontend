import axios, {
  AxiosHeaders,
  type AxiosInstance,
  type AxiosResponse,
} from "axios";
import { useAuthStore } from "@/stores/auth";
import type { ErrorResponse, Feed, FeedEntry, User } from "@/model";

const sharedHeaders: AxiosHeaders = new AxiosHeaders({
  "Content-Type": "application/json",
});

const backendURL: string = `/api/v1/`;

// Has to be seperate function to load token with SPA
function CreateNew(): AxiosInstance {
  const store = useAuthStore();
  if (store.isLoggedIn()) {
    sharedHeaders["Authorization"] = `Bearer: ${store.tokenString}`;
  }
  return axios.create({
    //baseURL: `api/v1/`,
    baseURL: backendURL,
    headers: sharedHeaders,
  });
}

export async function getFeeds(): Promise<Feed[]> {
  const response = await CreateNew().get<Feed[]>("feed");
  return response.data;
}

export async function getFeed(feedID: string): Promise<Feed> {
  const response = await CreateNew().get<Feed>(`feed/${feedID}`);
  return response.data;
}

export async function addFeed(feed: Feed): Promise<AxiosResponse<Feed>> {
  return CreateNew().post<Feed>("feed", feed);
}

export async function deleteFeed(
  feedID: string
): Promise<AxiosResponse<string>> {
  return CreateNew().delete(`feed/${feedID}`);
}

export async function login(
  username: string,
  password: string
): Promise<AxiosResponse<string>> {
  const json = JSON.stringify({
    name: username,
    password: password,
  });
  return CreateNew().post<string>("user/login", json);
}

export async function register(user: User): Promise<AxiosResponse<User>> {
  return CreateNew().post<User>("user", user);
}

export async function getEntries(feedID: string): Promise<FeedEntry[]> {
  const response = await CreateNew().get<FeedEntry[]>(`feed/${feedID}/entries`);
  return response.data;
}

export async function setReadEntry(
  feedID: string,
  entryID: string | number
): Promise<AxiosResponse> {
  return await CreateNew().patch(`feed/${feedID}/entries/${entryID}`);
}

export async function getAllUnreadEntries(): Promise<FeedEntry[]> {
  const response = await CreateNew().get<FeedEntry[]>(`feed/unread_entries`);
  return response.data;
}

export async function updateSelfUser(user: User): Promise<AxiosResponse<User>> {
  return CreateNew().patch<User>("user", user);
}

export async function getSelfUser(): Promise<AxiosResponse<User>> {
  return await CreateNew().get<User>(`user/self`);
}

export async function exportFeeds(): Promise<AxiosResponse<Blob>> {
  return CreateNew().get<Blob>("feed/export");
}

export async function importFeeds(
  file: File
): Promise<AxiosResponse<ErrorResponse>> {
  const formData = new FormData();
  formData.append("file", file);
  return CreateNew().post<ErrorResponse>("feed/import", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}
