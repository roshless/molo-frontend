import { useAuthStore } from "@/stores/auth";
import TheNavbar from "../TheNavbar.vue";
import { createPinia, setActivePinia } from "pinia";

describe("Navbar", () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  it("simple mount", () => {
    cy.mount(TheNavbar);
  });

  // it("mobile- test show burger", () => {
  //   cy.mount(TheNavbar);
  //   cy.get('[data-test="burger-navbar"]').click();
  //   cy.get('[data-test="burger-navbar"]').should("have.class", "is-active");
  // });

  // it("fullhd- test hidden burger", () => {
  //   cy.viewport(1920, 1080);

  //   cy.mount(TheNavbar);
  //   cy.get('[data-test="burger-navbar"]').not(".is-active");
  // });

  it("not logged in", () => {
    cy.mount(TheNavbar);
    cy.get("nav").contains("Sign Up");
    cy.get("nav").contains("Log in");
  });

  it("logged in", () => {
    const store = useAuthStore();
    store.setToken(
      "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTUwODg1ODcsImlhdCI6MTcxNDgyOTM4NywiaXNBZG1pbiI6dHJ1ZSwibmFtZSI6ImFkbWluIiwic3ViIjoiMSJ9.JafpdNSriPDrQeTKkja8YwDDOTsoGEOdonZe3P3YMrpnJjwA_vmdTpnIgJdA__XqNJ7E1PGKHeSixmi-azPmmg"
    );

    cy.mount(TheNavbar);
    cy.get("nav").contains("Add");
    cy.get("nav").contains("Feeds list");
    cy.get("nav").contains("Settings");
    cy.get("nav").contains("Logout");
  });
});
