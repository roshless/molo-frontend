import { setActivePinia, createPinia } from "pinia";
import { useAuthStore } from "../auth";

describe("Auth store", () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  const expiredToken =
    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTUwODg1ODcsImlhdCI6MTcxNDgyOTM4NywiaXNBZG1pbiI6dHJ1ZSwibmFtZSI6ImFkbWluIiwic3ViIjoiMSJ9.JafpdNSriPDrQeTKkja8YwDDOTsoGEOdonZe3P3YMrpnJjwA_vmdTpnIgJdA__XqNJ7E1PGKHeSixmi-azPmmg";

  it("set token", () => {
    const token = expiredToken;
    const store = useAuthStore();

    expect(store.tokenString).equals("");

    store.setToken(token);
    expect(store.tokenString).equals(token);
  });

  it("clear token", () => {
    const store = useAuthStore();
    store.setToken(expiredToken);
    store.clearToken();
    expect(store.tokenString).equals("");
  });

  it("check if logged in", () => {
    const store = useAuthStore();
    store.setToken(expiredToken);
    expect(store.isLoggedIn()).equals(true);

    store.clearToken();
    expect(store.isLoggedIn()).equals(false);
  });

  it("check if expired", () => {
    const store = useAuthStore();
    store.setToken(expiredToken);
    expect(store.isTokenExpired()).equals(true);
  });
});
