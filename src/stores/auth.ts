import { jwtDecode, type JwtPayload } from "jwt-decode";
import { defineStore } from "pinia";

interface auth {
  tokenString: String;
  token: TokenPayload;
}

interface TokenPayload extends JwtPayload {
  isAdmin: boolean;
  name: string;
}

export const useAuthStore = defineStore("authUser", {
  state: (): auth => ({
    tokenString: "",
    token: <TokenPayload>{},
  }),
  actions: {
    setToken(newToken: string) {
      this.token = jwtDecode(newToken);
      this.tokenString = newToken;
    },
    clearToken() {
      this.token = <TokenPayload>{};
      this.tokenString = "";
    },
    isLoggedIn(): boolean {
      return this.token.sub === "" || this.token.sub === undefined
        ? false
        : true;
    },
    isAdmin(): boolean {
      return this.token.isAdmin === true;
    },
    isTokenExpired(): boolean {
      const currentTimestamp = Math.floor(Date.now() / 1000);
      if (this.token.exp !== undefined && this.token.exp <= currentTimestamp) {
        return true;
      }
      return false;
    },
  },

  persist: true,
});
