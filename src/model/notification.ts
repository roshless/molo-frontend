export const NotificationLevel = {
  success: "success",
  info: "info",
  warning: "warning",
  error: "error",
} as const;

export type NotificationLevel = keyof typeof NotificationLevel;
