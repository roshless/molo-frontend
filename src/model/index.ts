import type { Duration } from "./frontend-specific";

export interface Feed {
  id?: number;
  user_id?: number;
  user?: string;
  category_id?: number;
  category: string;
  name?: string;
  author?: string;
  url?: string;
  rss_url: string;
  create_date?: Date;
  update_date?: Date;
}

export interface User {
  id?: number;
  name: string;
  email: string;
  password: string;
  is_admin?: boolean;
  update_interval: Duration;
}

export interface FeedEntry {
  id: number;
  feed_id: number;
  feed: string;
  user_id: number;
  user: string;
  name: string;
  url: string;
  content: string;
  date: Date;
  category_id: number;
  category: string;
  read: boolean;
}

/* 
https://timmousk.com/blog/typescript-interface-default-value/
*/

export interface ErrorResponse {
  error: String;
  status: number;
}
