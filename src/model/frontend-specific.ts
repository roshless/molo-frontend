// Duration default value is in nanoseconds
export class Duration {
  private _value: number = 0;

  constructor(value: number | string) {
    switch (typeof value) {
      // value in minutes
      case "string":
        this._value = parseInt(value) * 60000000000;
        break;

      // value in nanoseconds
      case "number":
        this._value = value;
        break;

      default:
        break;
    }
  }

  // Get nanosecond value
  get value(): number {
    return this._value / 60000000000;
  }

  // Set nanosecond value
  set value(value: number) {
    this._value = value * 60000000000;
  }

  fromNs(value: number) {
    this._value = value;
  }

  toJSON(): number {
    return this._value as number;
  }

  fromJSON(value: string) {
    this._value = parseInt(value) / 60000000000;
  }
}
