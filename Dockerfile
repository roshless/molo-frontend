FROM node:19-alpine as builder
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.20.2-alpine
COPY --from=builder /app/dist /usr/share/nginx/app
COPY nginx.conf /etc/nginx/conf.d/default.conf
